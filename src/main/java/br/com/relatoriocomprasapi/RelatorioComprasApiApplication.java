package br.com.relatoriocomprasapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelatorioComprasApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RelatorioComprasApiApplication.class, args);
	}

}
